import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { Player } from 'src/interfaces/Player';
import { AppService } from 'src/services/app.service';
import { Router, NavigationEnd } from '@angular/router';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
    private country: string;

    public tableDataSource: MatTableDataSource<Player>;
    private players: Player[];

    public columns = [
        { label: 'Local rank', value: 'local_country_rank' },
        { label: 'Name', value: 'name' },
        { label: 'Live pp', value: 'live_pp' },
        { label: 'Local pp', value: 'local_pp' },
        { label: 'pp change', value: 'pp_change' },
        { label: 'Live rank', value: 'live_country_rank' },
        { label: 'Rank change', value: 'country_rank_change' }
    ];

    public displayedColumns = this.columns.map(column => column.value);

    public showUnrankedPlayers: boolean;

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    constructor(private appService: AppService, private router: Router) { }

    public async ngOnInit(): Promise<void> {
        this.router.events.subscribe(async event => {
            if (event instanceof NavigationEnd) {
                (<any>window).ga('set', 'page', event.urlAfterRedirects);
                (<any>window).ga('send', 'pageview');

                this.setCountry();

                this.players = await this.appService.getPlayers(this.country, false);
                this.tableDataSource = new MatTableDataSource<Player>();
                this.tableDataSource.data = this.players;
                this.tableDataSource.paginator = this.paginator;
                this.tableDataSource.sort = this.sort;
            }
        });
    }

    public async toggleUnrankedPlayers(): Promise<void> {
        this.players = await this.appService.getPlayers(this.country, this.showUnrankedPlayers);
        this.tableDataSource.data = this.players;
    }

    private setCountry(): void {
        const url = this.router.url;
        const country = url.split('/')[1];
        this.country = country;
    }
}
