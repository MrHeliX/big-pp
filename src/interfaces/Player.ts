export interface Player {
    user_id: number,
    name: string,
    local_pp: number,
    live_pp: number,
    local_country_rank: number,
    live_country_rank: number,
    pp_change: number,
    country_rank_change: number
}