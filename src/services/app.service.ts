import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { Player } from 'src/interfaces/Player';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class AppService {
    constructor(private http: HttpClient) { }

    public getPlayers(country: string, includeUnrankedPlayers: boolean): Promise<Player[]> {
        return new Promise(resolve => {
            this.http.get<Player[]>(`${environment.API}/bigpp/players/${country}/?includeUnranked=${includeUnrankedPlayers}`).subscribe(response => {
                resolve(response.map((player, index) => {
                    return {
                        ...player,
                        local_country_rank: index + 1,
                        country_rank_change: player.live_country_rank - (index + 1)
                    };
                }));
            });
        });
    }
}